# Definition of done (DOD)

Im Folgenden wird die DOD definiert. Sie bestimmt wann ein UseCase als abgeschlossen gilt. Für die Durchführbarkeit (Proof of concept) des Projekts
ist die DOD entsprechend anzupassen. Diese Einschränkung wird mit den entsprechenden Teilgruppen in Rücksprache festgehalten. Die DOD gilt komplett für das Live-System. 

Die DOD setzt sich aus folgenden Teilbereichen zusammen:

1. Definierter UseCase
2. Definierte Testfälle zum UseCase
3. Automatische Tests (Unit-Tests, Integration-Tests, System-Tests)
4. ggf. manuelle Tests
5. Dokumentaion
6. Freigabe vom UseCase

## Definierter UseCase

Der zu implementierene UseCase muss in einem Dokument (z.B. Lastenheft) beschrieben werden.

## Definierte Testfälle zum UseCase

Zu jedem UseCase /LF???/ gibt es entsprechene Testfälle /T???/, die durch eine Testanforderungsdatei dokumentiert sind.

Die Testanforderungsdatei enthält sämtliche Tests (manuelle und automatische).

Das Verzeichnis für Testanforderungsdateien liegt im Repository : https://gitlab.com/2019ss_swtpj_group/hcam_doku

Die Verzeichnisstruktur für die Dateien ist wie folgt:

Tests (Wurzelverzeichnis)
 - T???.md|.lyx
 - (...)

**Beispiel:** 

UseCase: /LF10/Benutzer anlegen

Test: /T10/Benutzer anlegen

Testanforderungsdatei: T10_Benutzer_anlegen.md

Um die Implemtierung eines automatischen Tests zu zeigen wird das Sysmbol "[ ]" benutzt.

Zwei Fälle sind zu unterscheiden:

1. Fall: [X] , bedeutet der Test ist implementiert.
2. Fall: [ ] , bedeutet der Test ist geplant und noch nicht implementiert.

**Beispiel:**

[ ] Ein Benutzer wird neu angelegt mit allen Feldern ausgefüllt

[X] Ein Asset wird gelöscht

Um den Erfolg des manuellen Tests zu zeigen, wird das Symbol "[ ]" benutzt und außerdem ein "letztes Testdatum" angegeben.

Falls kein Datum angegeben ist, ist der Test nur geplant.

Folgendes Muster ist einzuhalten: [ ] Testbeschreibung , letztes Testdatum

**Beispiel:**

[  ] Es soll eine Notification kommen wenn Bluetooth-Beacon mit Auftrag in der Nähe ist. 5.5.2019

[X] Die UUID vom Beacon wird angezeigt. 5.5.2019

Die Vorlage zu einer Testanforderungsdatei ist im Abschnitt "Vorlagen" zu finden.

## Tests

Um die Qualität der Software zu gewähren, wird die Funktionalität stets durch automatische bzw. manuelle Tests geprüft.

Die Priorität der Tests ist wie folgt, wobei eine Priorität von 3 wichtiger als die Priorität von 1 ist.

1. Manueller-Test
2. Unit-Test
3. Integrations-Test
4. System-Test

### Automatische Tests

Die Testfälle werden vor und während der Implementierungsphase geschrieben, nicht im Anschluss an diese. 

Für  jeden  entdeckten  Fehler  während  der  Entwicklung  empfielt  es  sich,  mindestens  einen
neuen Testfall zu erstellen, damit dieser Fehler zu einem späteren Zeitpunkt nicht unbemerkt
wieder entsteht.

#### Unit-Tests

Die Unit-Tests werden innerhalb der Teilgruppen (z.B. Frontend, Backend) geschrieben, da diese Gruppen mit dem eigenen Quellcode am vertrautesten sind.

Der Umfang der zu erstellenden Unit-Tests ergibt sich aus folgenden Regeln:

1. Zu jeder Klasse muss mindestens eine Unit-Testklasse existieren, die die korrekte Funktionsweise der Methoden überprüft (außer für reine GUI-Klassen)
 
2. Jede  Methode,  die  nicht  ausschließlich  das  Setzen  oder  Auslesen  eines  einzelnen  Attributs
realisiert, muss durch mindestens einen positiven (happy path) und einen negativen (error path) eigenen Testfall geprüft werden.

3. Methoden, die ausschließlich eine grafische Funktion haben, müssen nicht getestet werden.

#### Integration-Tests

Die Integration-Tests werden von der Test-Gruppe geschrieben, da diese Gruppe verantwortlich für die Integration vom Frontend und Backend ist.

Der Umfang der zu erstellenden Integration-Tests ergibt sich aus folgenden Regeln:

1. Für jede nicht triviale zu integrierende Schnittstelle muss mindestens eine Integrations-Testklasse existieren, die die korrekte Funktionsweise der Schnittstelle überprüft.
 
2. Jede nicht triviale Schnittstelle muss durch mindestens einen positiven (happy path) und einen negativen (error path) eigenen Testfall geprüft werden.

#### System-Tests

Die System-Tests werden von der Test-Gruppe geschrieben, da diese Gruppe verantwortlich für die Integration vom Frontend und Backend ist.
Es werden primär End-zu-End-Tests formuliert. Diese stellen die Funktionalität durch alle Schichten des Systems sicher.
Beispielsweise wird in der Darstellungsschicht ein Prozess so angestoßen, dass in der Persistenzschicht neue Daten gespeichert werden.

Der Umfang der zu erstellenden System-Tests ergibt sich aus folgenden Regeln:

1. Für jedes nicht triviales Verhalten muss mindestens ein System-Test existieren, der die korrekte Funktionsweise überprüft.
 
2. Jedes nicht triviales Verhalten muss durch mindestens einen positiven (happy path) und einen negativen (error path) eigenen Testfall geprüft werden.

#### Manuelle-Tests

Wenn die Funktionalität der Software nicht automatisch geprüft werden kann, dann muss sie manuell getestet werden. Beispielsweise
bei bestimmten Verhalten der Hardware, welches nur schlecht oder gar nicht in Software simuliert werden kann. Die Manuellen-Tests werden von der entsprechenden Teilgruppe geschrieben (z.B. Frontend), 
da diese Gruppe verantwortlich für die korrekte Funktionalität ist.

Der Umfang der zu erstellenden Manuellen-Tests ergibt sich aus folgenden Regeln:

1. Für jedes nicht triviales Verhalten muss mindestens ein Manueller-Test existieren, der die korrekte Funktionsweise überprüft.
 
2. Jedes nicht triviales Verhalten muss durch mindestens einen positiven (happy path) und einen negativen (error path) eigenen Testfall geprüft werden.


## Dokumentaion

Die Dokumentaion wird vor und während der Implementierungsphase geschrieben, nicht im Anschluss an diese. Die Dokumentation setzt sich wie folgt zusammen:

1. UseCase Beschreibung
2. Testanfoderungsdatei
3. Quellcode Dokumentation in abhängigkeit zur Technologie (z.B. Javadoc)
4. Ergebnisse einpflegen in endgültige Kundendokumentation


## Freigabe vom UseCase

Schlussendlich erfolgt im letzten Schritt die Freigabe. Diese erfolgt durch den Verantwortlichen der Endabnahme.
Der Verantwortliche ist eine Person, die nicht aktiv an der Entwicklung einer Funktionalität beteiligt ist.
Bei der Freigabe wird unteranderem durch die Berücksichtigung der DOD eine Funktionalität der Software abgeschlossen.

## Vorlagen

### Testanforderungsdatei

UseCase:
        
Akzeptanzkriterien:
    
# 1. Test-Team
    
Autoren:
    
## Automatische-Tests

### Unit-Tests
    
### Integrations-Tests
    
### System-Tests

## Manuelle-Tests
    
# 2. Frontend-Team
    
Autoren:
        
## Automatische-Tests

### Unit-Tests
    
### Integrations-Tests
    
### System-Tests

## Manuelle-Tests
    
# 3. Backend-Team
    
Autoren:
     
## Automatische-Tests

### Unit-Tests
    
### Integrations-Tests
    
### System-Tests

## Manuelle-Tests
    