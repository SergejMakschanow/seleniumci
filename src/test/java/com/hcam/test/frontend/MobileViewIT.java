package com.hcam.test.frontend;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import com.consol.citrus.dsl.testng.TestNGCitrusTestRunner;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MobileViewIT extends TestNGCitrusTestRunner {

    //Autowired bugged in Intellj
    @Autowired
    private SeleniumBrowser browser;

    @Autowired
    private HttpClient httpClient;

    @DataProvider
    public Object[][] assignmentsData(){
        return new Object[][]{
                {"Lokale Aufträge", "#id1"},
                {"Aktuelle Aufträge", "#id1"}
        };
    }

    public void startBrowser(){

        selenium(seleniumActionBuilder -> seleniumActionBuilder
                .browser(browser)
                .start());

        selenium(seleniumActionBuilder -> seleniumActionBuilder
                .navigate(httpClient.getEndpointConfiguration().getRequestUrl()));

    }

    @Test
    @CitrusTest
    public void assignmentsTest(String location, String id){

        startBrowser();

        selenium(seleniumActionBuilder -> seleniumActionBuilder.click().element(By.linkText(location)));

        selenium(seleniumActionBuilder -> seleniumActionBuilder.click().element(By.id(id)));

        selenium(seleniumActionBuilder -> seleniumActionBuilder.find().element(By.className("details-collapse collapse show")));


    }




}
