package com.hcam.test.frontend.webpages;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import com.consol.citrus.selenium.model.PageValidator;
import com.consol.citrus.selenium.model.WebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DesktopPage implements WebPage, PageValidator<DesktopPage> {

    @FindBy(xpath = "//div[contains(text(),'Logout')]")
    private WebElement logout;

    @FindBy(xpath = "//div[contains(text(),'User')]")
    private WebElement user;

    @FindBy(xpath = "//div[contains(text(),'Tasks')]")
    private WebElement tasks;

    @FindBy(xpath = "//div[contains(text(),'Task templates')]")
    private WebElement taskTemplates;

    @FindBy(xpath = "//div[contains(text(),'Tags')]")
    private WebElement tags;

    @FindBy(xpath = "//div[contains(text(),'Assets')]")
    private WebElement assets;

    @FindBy(xpath = "//div[contains(text(),'Create')]")
    private WebElement create;


    public void checkCollapse(String collapse) throws InterruptedException {
        switch(collapse){
            case "user" :
                user.click();
                Thread.sleep(1000);
                user.findElement(By.xpath("//div[contains(text(),'Create')]")).click();

                break;

            case "tasks" :
                tasks.click();
                tasks.findElement(By.xpath("//div[contains(text(),'Create')]"));
                break;

            case "task templates" :
                taskTemplates.click();
                taskTemplates.findElement(By.xpath("//div[contains(text(),'Create')]"));
                break;

            case "tags" :
                tags.click();
                tags.findElement(By.xpath("//div[contains(text(),'Create')]"));
                break;

            case "assets" :
                assets.click();
                assets.findElement(By.xpath("//div[contains(text(),'Create')]"));
                break;
        }

        Thread.sleep(1000);



    }


    public void logout(){
        logout.click();
    }

    @Override
    public void validate(DesktopPage desktopPage, SeleniumBrowser seleniumBrowser, TestContext testContext) {

    }
}
