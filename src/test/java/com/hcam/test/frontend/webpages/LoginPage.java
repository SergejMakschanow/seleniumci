package com.hcam.test.frontend.webpages;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import com.consol.citrus.selenium.model.PageValidator;
import com.consol.citrus.selenium.model.WebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage implements WebPage, PageValidator<LoginPage> {

    /*@FindBy(tagName = "from-signin")
    private WebElement signinForm;*/

    @FindBy(id = "inputUsername")
    private WebElement inputUsername;

    @FindBy(id = "inputPassword")
    private WebElement inputPassword;

    @FindBy(linkText = "Login")
    private WebElement login;

    @FindBy(id = "test-isWebview")
    private WebElement webViewActive;

    @FindBy(id = "test-isUseRestLogin")
    private WebElement restApiActive;




    public void loginInput(String userName, String password, String desktopView) throws InterruptedException {
        //Thread.sleep(1000);
        if(desktopView.equals("true")){
            //webViewActive.findElement(By.id("test-isWebview")).click();
            //restApiActive.click();
        }
        Thread.sleep(1000);
        inputUsername.sendKeys(userName);
        inputPassword.sendKeys(password);

        Thread.sleep(2000);



    }

    public void clickLogin() throws InterruptedException {
        login.click();
        Thread.sleep(2000);
    }

    @Override
    public void validate(LoginPage loginPage, SeleniumBrowser seleniumBrowser, TestContext testContext) {

        //TODO: validate
    }
}
