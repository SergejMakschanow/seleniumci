package com.hcam.test.frontend.webpages;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import com.consol.citrus.selenium.model.PageValidator;
import com.consol.citrus.selenium.model.WebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CreateUserPage implements WebPage, PageValidator<CreateUserPage> {

    @FindBy(id = "username")
    private WebElement inputUsername;

    @FindBy(id = "password")
    private WebElement inputPassword;

    @FindBy(id = "forename")
    private WebElement inputForname;

    @FindBy(id = "surname")
    private WebElement inputSurname;

    @FindBy(id = "email")
    private WebElement inputEmail;

    @FindBy(id = "phone")
    private WebElement inputPhone;

    @FindBy(id = "department")
    private WebElement inputDepartment;

    @FindBy(id  = "role")
    private WebElement inputRole;

    @FindBy(xpath = "//button[contains(text(),'Create')]")
    private WebElement create;

    @FindBy(xpath = "//div[contains(text(),'Abort')]")
    private WebElement abort;

    public void inputUserData(String username, String password, String forename, String surname, String email, String phone, String department, String role){
        inputUsername.sendKeys(username);
        inputPassword.sendKeys(password);
        inputForname.sendKeys(forename);
        inputSurname.sendKeys(surname);
        inputEmail.sendKeys(email);
        inputPhone.sendKeys(phone);
        inputDepartment.sendKeys(department);
        Select dropDownRole = new Select(inputRole);
        dropDownRole.selectByValue(role);
    }

    public void acceptCreation(){
        create.click();
    }

    public void declineCreation(){
        abort.click();
    }

    @Override
    public void validate(CreateUserPage createUserPage, SeleniumBrowser seleniumBrowser, TestContext testContext) {

    }
}
