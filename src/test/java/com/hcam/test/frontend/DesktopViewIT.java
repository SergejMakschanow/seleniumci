package com.hcam.test.frontend;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import com.consol.citrus.selenium.model.WebPage;
import com.hcam.test.frontend.webpages.CreateUserPage;
import com.hcam.test.frontend.webpages.DesktopPage;
import com.hcam.test.frontend.webpages.LoginPage;
import org.springframework.beans.factory.annotation.Autowired;


import org.testng.annotations.*;


public class DesktopViewIT extends TestNGCitrusTestDesigner {

    private String user = "yvasylenko";
    private String password = "password";

    //Autowired bugged in Intellj
    @Autowired
    private SeleniumBrowser browser;

    @Autowired
    private HttpClient httpClient;

    @DataProvider
    public Object[][] collapseTestData() {
        return new Object[][] {
                {new CreateUserPage(), "user" },
                {new CreateUserPage(), "tasks", },
                {new CreateUserPage(), "taskTemplates"},
                {new CreateUserPage(), "assetsGroupCollapse"}
        };
    }

    @DataProvider
    public Object[][] menuPointTestData(){
        return  new Object[][] {
                {"Export", "Exports"},
                {"Backup", "Backups"}
        };
    }




    @Test(dataProvider = "collapseTestData")
    @CitrusTest
    public void collapseTest(WebPage webPage, String collapse){

        variable("userName", "yvasylenko");
        variable("password", "password");
        variable("desktopView", true);
        variable("collapse", collapse);

        LoginPage loginPage = new LoginPage();

        selenium().page(loginPage)
                .argument("${userName}")
                .argument("${password}")
                .argument("${desktopView}")
                .execute("loginInput");



        selenium().page(loginPage).execute("clickLogin");

        DesktopPage desktopPage = new DesktopPage();

        selenium().page(desktopPage).validate();

        selenium().page(desktopPage)
                .argument("${collapse}")
                .execute("checkCollapse");

        selenium().page(webPage)
                .validate();

    }

}
