package com.hcam.test.frontend;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.container.SequenceAfterTest;
import com.consol.citrus.dsl.design.TestDesigner;
import com.consol.citrus.dsl.design.TestDesignerAfterTestSupport;
import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import com.consol.citrus.dsl.testng.TestNGCitrusTestRunner;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import com.hcam.test.frontend.webpages.CreateUserPage;
import com.hcam.test.frontend.webpages.DesktopPage;
import com.hcam.test.frontend.webpages.LoginPage;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DesktopST extends TestNGCitrusTestDesigner {



    @Autowired
    private SeleniumBrowser browser;

    @Autowired
    private HttpClient httpClient;


    @DataProvider
    public Object[][] positiveLoginData(){
        return new Object[][]{
                {"yvasylenko", "password"}
        };
    }

    @DataProvider
    public Object[][] negativLoginData(){
        return new Object[][]{
                {"False", "false"}
        };
    }


    @DataProvider
    public Object[][] UserTestData() {
        return new Object[][] {
                {"HansTest", "pass", "Hans", "Test", "hanstest@gmail.com", "0456423665", "New", "3"}
        };
    }



    @DataProvider
    public Object[][] ChangeTestData() {
        return new Object[][] {

        };
    }



    @Test (dataProvider = "positiveLoginData")
    @CitrusTest
    public void posetivLoginTest(String userName, String password) {

        variable("userName", userName);
        variable("password", password);
        variable("desktopView", "true");


        LoginPage loginPage = new LoginPage();

        selenium().page(loginPage).validate();

        selenium().page(loginPage)
                .argument("${userName}")
                .argument("${password}")
                .argument("${desktopView}")
                .execute("loginInput");

        selenium().page(loginPage).execute("clickLogin");

        DesktopPage desktopPage = new DesktopPage();

        selenium().page(desktopPage).validate();

    }

    @Test (dataProvider = "negativLoginData" )
    @CitrusTest
    public void negativLoginTest(String userName, String password) {

        variable("userName", userName);
        variable("password", password);
        variable("desktopView", true);

        LoginPage loginPage = new LoginPage();

        selenium().page(loginPage).validate();

        selenium().page(loginPage)
                .argument("${userName}")
                .argument("${password}")
                .argument("${desktopView}")
                .execute("loginInput");

        selenium().page(loginPage).execute("clickLogin");

        selenium().alert().dismiss();

        selenium().alert().accept();

        selenium().page(loginPage).validate();
    }



    @Test (dataProvider = "UserTestData", dependsOnMethods = "posetivLoginTest")
    @CitrusTest
    public void posetivCreateUserTest(String userName, String password, String foreName, String surName, String email , String phone, String department , String role){

        variable("userName", userName);
        variable("password", password);
        variable("forename", foreName);
        variable("surname", surName);
        variable("email", email);
        variable("phone", phone);
        variable("department", department);
        variable("role", role);
        variable("collapse", "user");

        DesktopPage desktopPage = new DesktopPage();

        selenium().page(desktopPage)
                .argument("${collapse}")
                .execute("checkCollapse");

        CreateUserPage createUserPage = new CreateUserPage();

        selenium().page(createUserPage)
                .argument("${userName}")
                .argument("${password}")
                .argument("${forename}")
                .argument("${surname}")
                .argument("${email}")
                .argument("${phone}")
                .argument("${department}")
                .argument("${role}")
                .execute("inputUserData");

    }

    @Test
    @CitrusTest
    public void negativCreateTest(String userName, String password, String foreName, String surName, String email , String phone, String department ){


    }

    @Test
    @CitrusTest
    public void posetivChangeTest(){

    }

    @Test
    @CitrusTest
    public void negativChangeTest(){


    }



}
