package com.hcam.test.frontend;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.dsl.testng.TestNGCitrusTestRunner;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MobileST extends TestNGCitrusTestRunner {

    @Autowired
    private SeleniumBrowser browser;

    @Autowired
    private HttpClient httpClient;

    public void startBrowser(){
        selenium(seleniumActionBuilder -> seleniumActionBuilder
                .browser(browser)
                .start());

        selenium(seleniumActionBuilder -> seleniumActionBuilder
                .navigate(httpClient.getEndpointConfiguration().getRequestUrl()));

    }

    @DataProvider
    public Object[][] acceptTaskTestData() {
        return new Object[][] {

        };
    }

    @DataProvider
    public Object[][] completeTaskTestData() {
        return new Object[][] {

        };
    }

    @DataProvider
    public Object[][] cancelTaskTestData() {
        return new Object[][] {

        };
    }


    @Test
    @CitrusTest
    public void acceptTask(){
        startBrowser();
    }


    @Test
    @CitrusTest
    public void completeTask(){
        startBrowser();

    }

    @Test
    @CitrusTest
    public void cancelTask(){
        startBrowser();

    }


}
