package com.hcam.test.config;

import com.consol.citrus.container.SequenceAfterSuite;
import com.consol.citrus.container.SequenceAfterTest;
import com.consol.citrus.container.SequenceBeforeTest;
import com.consol.citrus.dsl.design.TestDesigner;
import com.consol.citrus.dsl.design.TestDesignerAfterTestSupport;
import com.consol.citrus.dsl.design.TestDesignerBeforeTestSupport;
import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.dsl.runner.TestRunner;
import com.consol.citrus.dsl.runner.TestRunnerAfterSuiteSupport;
import com.consol.citrus.dsl.runner.TestRunnerAfterTestSupport;
import com.consol.citrus.dsl.runner.TestRunnerBeforeSuiteSupport;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.selenium.endpoint.SeleniumBrowser;
import com.hcam.test.frontend.webpages.DesktopPage;
import org.apache.http.conn.ssl.*;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.auth.AuthScope;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.openqa.selenium.remote.BrowserType;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * @author Schiko
 */

@Configuration
public class EndpointConfig {

    @Value("/src/test/resources/keys/server.keystore")
    private String sslKeyStorePath;

    private static final String HOST = "localhost";

    private static final int PORT = 443;

    private static final String SECRET = "test123";

    private static final String KEYSTORE = "keys/server.keystore";

    private static final String USERNAME = "yvasylenko";

    private static final String PASSWORD = "password";

    public static final String BROWSER_HTML_UNIT = "browserHtmlUnit";

    @Bean
    public HttpClient client() {
        return CitrusEndpoints
            .http()
                .client()
                .requestUrl("https://" + this.HOST + ":" + this.PORT)
                .requestFactory(this.sslRequestFactory())
            .build();
    }

    @Bean
    public HttpComponentsClientHttpRequestFactory sslRequestFactory() {
        return new HttpComponentsClientHttpRequestFactory(this.httpsClient());
    }

    @Bean
    public org.apache.http.client.HttpClient httpsClient() {
        try {

	    final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

	    credentialsProvider.setCredentials(new AuthScope(this.HOST, this.PORT, AuthScope.ANY_REALM, AuthSchemes.BASIC), new UsernamePasswordCredentials(this.USERNAME, this.PASSWORD));

            final SSLContext sslcontext = SSLContexts
                .custom()
                    .loadTrustMaterial(new ClassPathResource(this.KEYSTORE).getFile(), this.SECRET.toCharArray(), new TrustSelfSignedStrategy())
                .build();

            final SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslcontext, NoopHostnameVerifier.INSTANCE);

            return HttpClients
                .custom()
                    .setSSLSocketFactory(sslSocketFactory)
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
		    .setDefaultCredentialsProvider(credentialsProvider)
                .build();
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            throw new BeanCreationException("Failed to create https client for ssl connection", e);
        }
    }

    @Bean
    public SeleniumBrowser browser() {
        System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver");
        return CitrusEndpoints
                .selenium()
                .browser()
                .type(BrowserType.CHROME)
                .build();
    }

    @Bean
    @DependsOn("browser")
    public SequenceAfterSuite afterSuite(SeleniumBrowser browser) {
        return new TestRunnerAfterSuiteSupport() {
            @Override
            public void afterSuite(TestRunner runner) {
                runner.selenium(builder -> builder.browser(browser).stop());
            }
        };
    }

    @Bean
    @DependsOn({"browser", "httpClient"})
    public SequenceBeforeTest beforeTest(SeleniumBrowser browser, HttpClient httpClient){
        return new TestDesignerBeforeTestSupport() {
            @Override
            public void beforeTest(TestDesigner testDesigner) {

                testDesigner.selenium().browser(browser).start();
                testDesigner.selenium().navigate(httpClient.getEndpointConfiguration().getRequestUrl());


            }
        };
    }


    @Bean
    @DependsOn("browser")
    public SequenceAfterTest afterTest(SeleniumBrowser browser) {
        return new TestDesignerAfterTestSupport() {
            @Override
            public void afterTest(TestDesigner runner) {
                //DesktopPage desktopPage = new DesktopPage();
                //runner.selenium().page(desktopPage).execute("logout");
                //runner.selenium().browser(browser).stop();
                runner.sleep(500);

            }
        };
    }

    @Bean
    public HttpClient httpClient() {
        return CitrusEndpoints
                .http()
                .client()
                .requestUrl("https://2019ss_swtpj_group.gitlab.io/hcam_frontend/")
                .build();
    }

    /*@Bean
    @Qualifier(EndpointConfig.BROWSER_HTML_UNIT)
    public SeleniumBrowser browserHtmlUnit() {
        return CitrusEndpoints
                .selenium()
                .browser()
                .startPage("https://www.2019ss_swtpj_group.gitlab.io/hcam_frontend/")
                .type(BrowserType.HTMLUNIT)
                .build();

                //.startPage("http://" + EndpointConfig.HOST + ":80/hcam_frontend")

    }*/

}

