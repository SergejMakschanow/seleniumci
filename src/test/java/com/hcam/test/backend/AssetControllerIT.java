package com.hcam.test.backend;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import com.consol.citrus.annotations.CitrusTest;
import org.springframework.http.HttpStatus;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Integration tests for the CRUD properties of the AssetController of the hcam_backend
 *
 * @author Schiko
 * @since 2019-05-07
 */
@Test
public class AssetControllerIT extends ControllerIT {

    public Object[][] positiveTestDataGetMethod() {
        return new Object[][] { 
		{ "assets" , "" , HttpStatus.OK , 1 },
		{ "assets/name/" , "Krankenbett" , HttpStatus.OK , 1 }, //TODO HttpStatus.INTERNAL_SERVER_ERROR when multiple assets with same name
		{ "assetTypes" , "" , HttpStatus.I_AM_A_TEAPOT , 1 } //TODO expected '418' but was '404'
        };
    }

    public Object[][] negativeTestDataGetMethod() {
        return new Object[][] { 
		{ "assets/name/" , "get-doesnotexist" , HttpStatus.NOT_FOUND , 1 } //TODO expected HttpStatus.NOT_FOUND but was HttpStatus.OK
	};
    }

    public Object[][] positiveTestDataPostMethod() {
        return new Object[][] { 
		{ "assets" , "{ \"name\": \"Krankenbett\", \"room\" : \"18\", \"building\" : \"1\", \"level\" : \"2\", \"operative\": false, \"inspected\": false, \"tags\": \"[ " +  UUID.randomUUID().toString() + "]\" }" , HttpStatus.CREATED } //TODO expect asset due to new rest-api spec, but asset object needs to be updated in backend and database
        };
    }

    public Object[][] negativeTestDataPostMethod() {
        return new Object[][] { 
		{ "assets" , "{}" , HttpStatus.INTERNAL_SERVER_ERROR }
        };
    }

    public Object[][] positiveTestDataPutMethod() {
        return new Object[][] { 
		{ "assets/" , "1" ,"{ \"name\": \"Krankenbett\", \"room\" : \"18\", \"building\" : \"1\", \"level\" : \"2\", \"operative\": false, \"inspected\": false, \"tags\": \"[ " +  UUID.randomUUID().toString() + "]\" }" , HttpStatus.ACCEPTED }, //TODO expected '202' but was '400'
		{ "assets/" , "Krankenbett" ,"{ \"name\": \"Krankenbett\", \"room\" : \"18\", \"building\" : \"1\", \"level\" : \"2\", \"operative\": false, \"inspected\": false, \"tags\": \"[ " +  UUID.randomUUID().toString() + "]\" }" , HttpStatus.I_AM_A_TEAPOT } //TODO expected '418' but was '400'
        };
    }

    public Object[][] negativeTestDataPutMethod() {
        return new Object[][] { 
		{ "assets/" , "-1" ,"{}" , HttpStatus.I_AM_A_TEAPOT } //TODO expected '418' but was '500'
        };
    }

    public Object[][] positiveTestDataDeleteMethod() {
        return new Object[][] { 
		{ "assets/" , "test-delete-asset" , "{ \"name\": \"test-delete-asset\", \"room\" : \"18\", \"building\" : \"1\", \"level\" : \"2\", \"operative\": false, \"inspected\": false, \"tags\": \"[ " +  UUID.randomUUID().toString() + "]\" }" , HttpStatus.ACCEPTED }, //TODO raises HttpStatus.INTERNAL_SERVER_ERROR
		{ "assets/" , "1" , "" , HttpStatus.I_AM_A_TEAPOT } // TODO expected '418' but was '500'
        };
    }

    public Object[][] negativeTestDataDeleteMethod() {
        return new Object[][] { 
		{ "assets/" , "doesnotExistsKrankenbett" , "{\"idAsset\": \"2\",\"name\": \"Krankenbett\",\"inspected\": \"0\",\"operative\": 0,\"Position_idPosition\": 0,\"AssetType_idAssetType\": \"0\" }" , HttpStatus.INTERNAL_SERVER_ERROR }
        };
    }

    @DataProvider(name = "get-test-data")
    public Object[][] dataProviderGetMethod() {
	return super.mergeTestData(this.positiveTestDataGetMethod(), this.negativeTestDataGetMethod());	
    }

    @DataProvider(name = "post-test-data")
    public Object[][] dataProviderPostMethod() {
	return super.mergeTestData(this.positiveTestDataPostMethod(), this.negativeTestDataPostMethod());	
    }

    @DataProvider(name = "put-test-data")
    public Object[][] dataProviderPutMethod() {
	return super.mergeTestData(this.positiveTestDataPutMethod(), this.negativeTestDataPutMethod());	
    }

    @DataProvider(name = "delete-test-data")
    public Object[][] dataProviderDeleteMethod() {
	return super.mergeTestData(this.positiveTestDataDeleteMethod(), this.negativeTestDataDeleteMethod());	
    }

    @CitrusTest
    @Test(dataProvider = "post-test-data")
    public void post(final String endpoint, final String payload, final HttpStatus expectedResponse){
	super.post(endpoint, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "get-test-data")
    public void get(final String endpoint, final String endpointParameter, final HttpStatus expectedResponse, final Integer expectedElements){
	super.get(endpoint, endpointParameter, expectedResponse, expectedElements);
    }

    @CitrusTest
    @Test(dataProvider = "put-test-data")
    public void put(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.put(endpoint, endpointParameter, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "delete-test-data")
    public void delete(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.delete(endpoint, endpointParameter, payload, expectedResponse);
    }

}
