package com.hcam.test.backend;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import com.consol.citrus.annotations.CitrusTest;
import org.springframework.http.HttpStatus;

/**
 * Integration tests for the CRUD properties of the UserController of the hcam_backend
 *
 * @author Schiko
 * @since 2019-05-07
 */
@Test
public class UserControllerIT extends ControllerIT {

    public Object[][] positiveTestDataGetMethod() {
        return new Object[][] { 
		{ "users" , "" , HttpStatus.OK , 1 },
		{ "users/department/" , "System%20Admin" , HttpStatus.OK , 2 }, 
		{ "users/group/" , "1" , HttpStatus.OK , 1 }, //TODO remove endpoint
		{ "users/email/" , "maelzer@uksh.de" , HttpStatus.OK, 1 }, //TODO remove endpoint
		{ "users/forename/" , "Anton" , HttpStatus.OK , 1 }, 
		{ "users/surname/" , "Admin" , HttpStatus.OK , 1 },
		{ "users/phone/" , "+49123456" , HttpStatus.INTERNAL_SERVER_ERROR , 1 }, //TODO No valid state
		{ "users/username/" , "boss" , HttpStatus.INTERNAL_SERVER_ERROR , 1 }, //TODO No valid state
		{ "roles/" , "" , HttpStatus.OK , 1 }, //TODO expected '200' but was '404'
	        { "users/departments" , "", HttpStatus.OK , 1 } //TODO expected '200' but was '404'
        };
    }

    public Object[][] negativeTestDataGetMethod() {
        return new Object[][] { 
        	{ "users/username/" , "doesnotexist" , HttpStatus.INTERNAL_SERVER_ERROR , 0 }
	};
    }


    public Object[][] positiveTestDataPostMethod() {
        return new Object[][] { 
		{ "users" , "{ \"username\": \"test-post-user\", \"surname\": \"Admin\", \"forename\": \"Anton\", \"email\": \"test@admin.de\", \"phone\": \"+49123456\", \"department\": \"Administration\", \"idRole\": 1, \"password\": \"123456\", \"tasks\" : [] }", HttpStatus.CREATED } 
        };
    }

    public Object[][] negativeTestDataPostMethod() {
        return new Object[][] { 
		{ "users" , "{}", HttpStatus.BAD_REQUEST }
        };
    }


    public Object[][] positiveTestDataPutMethod() {
        return new Object[][] { 
		{ "users/", "test-put-user", "{ \"idUser\": 1, \"surname\": \"Admin\", \"forename\": \"Anton\", \"email\": \"test@admin.de\", \"phone\": \"+49123456\", \"department\": \"Administration\", \"idRole\": 1, \"password\": \"123456\", \"tasks\" : [] }", HttpStatus.OK }
        };
    }

    public Object[][] negativeTestDataPutMethod() {
        return new Object[][] { 
		{ "users/" , "@", "{\"username\": \"\"}", HttpStatus.INTERNAL_SERVER_ERROR } 
        };
    }


    public Object[][] positiveTestDataDeleteMethod() {
        return new Object[][] { 
		{ "users/", "test-delete-user" , "{ \"username\": \"test-delete-user\", \"surname\": \"Admin\", \"forename\": \"Anton\", \"email\": \"test@admin.de\", \"phone\": \"+49123456\", \"department\": \"Administration\", \"idRole\": 1, \"password\": \"123456\", \"tasks\" : [] }", HttpStatus.OK } 
        };
    }

    public Object[][] negativeTestDataDeleteMethod() {
        return new Object[][] { 
		{ "users/" , "doesnotexist", "{}" , HttpStatus.INTERNAL_SERVER_ERROR}
        };
    }

    @DataProvider(name = "get-test-data")
    public Object[][] dataProviderGetMethod() {
	return super.mergeTestData(this.positiveTestDataGetMethod(), this.negativeTestDataGetMethod());	
    }

    @DataProvider(name = "post-test-data")
    public Object[][] dataProviderPostMethod() {
	return super.mergeTestData(this.positiveTestDataPostMethod(), this.negativeTestDataPostMethod());	
    }

    @DataProvider(name = "put-test-data")
    public Object[][] dataProviderPutMethod() {
	return super.mergeTestData(this.positiveTestDataPutMethod(), this.negativeTestDataPutMethod());	
    }

    @DataProvider(name = "delete-test-data")
    public Object[][] dataProviderDeleteMethod() {
	return super.mergeTestData(this.positiveTestDataDeleteMethod(), this.negativeTestDataDeleteMethod());	
    }

    @CitrusTest
    @Test(dataProvider = "post-test-data")
    public void post(final String endpoint, final String payload, final HttpStatus expectedResponse){
	super.post(endpoint, payload, expectedResponse);
    }
    @CitrusTest
    @Test(dataProvider = "get-test-data")
    public void get(final String endpoint, final String endpointParameter, final HttpStatus expectedResponse, final Integer expectedElements){
	super.get(endpoint, endpointParameter, expectedResponse, expectedElements);
    }

    @CitrusTest
    @Test(dataProvider = "put-test-data")
    public void put(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.put(endpoint, endpointParameter, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "delete-test-data")
    public void delete(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.delete(endpoint, endpointParameter, payload, expectedResponse);
    }

}
