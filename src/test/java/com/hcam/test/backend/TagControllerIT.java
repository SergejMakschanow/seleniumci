package com.hcam.test.backend;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import com.consol.citrus.annotations.CitrusTest;
import org.springframework.http.HttpStatus;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Integration tests for the CRUD properties of the TagController of the hcam_backend
 *
 * @author Schiko
 * @since 2019-05-07
 */
@Test
public class TagControllerIT extends ControllerIT {

    public Object[][] positiveTestDataGetMethod() {
        return new Object[][] { 
		{ "tags" , "" , HttpStatus.OK , 1 } ,
		{ "tags/active" , "" , HttpStatus.OK , 1 },
		{ "tags/inactive" , "" , HttpStatus.OK , 1 }, //TODO expected 'a value equal to or greater than <1>' but was '0'
		{ "tags/category" , "1" , HttpStatus.OK , 1 } // TODO expected '200' but was '404'
        };
    }

    public Object[][] negativeTestDataGetMethod() {
        return new Object[][] { 

	};
    }

    public Object[][] positiveTestDataPostMethod() {
        return new Object[][] { 
		{ "tags" , "{ \"idTag\": \"0\", \"uuid\" : \""+UUID.randomUUID().toString()+"\", \"category\" : \"1\", \"active\" : true }" , HttpStatus.CREATED } //TODO expected '201' but was '400'
        };
    }

    public Object[][] negativeTestDataPostMethod() {
        return new Object[][] { 
		{ "tags" , "{}" , HttpStatus.INTERNAL_SERVER_ERROR } //TODO no valid response and method is public (not just admin)
        };
    }

    public Object[][] positiveTestDataPutMethod() {
        return new Object[][] { 
		{ "tags/" , "0", "{ \"idTag\": \"0\", \"uuid\" : \""+UUID.randomUUID().toString()+"\", \"category\" : \"1\", \"active\" : true }" , HttpStatus.OK } // TODO expected '200' but was '400'
        };
    }

    public Object[][] negativeTestDataPutMethod() {
        return new Object[][] { 
		{ "tags/" , "-1", "{ \"idTag\": \"0\", \"uuid\" : \""+UUID.randomUUID().toString()+"\", \"category\" : \"1\", \"active\" : true }" , HttpStatus.NOT_FOUND }  // TODO expected '404' but was '400'
        };
    }

    public Object[][] positiveTestDataDeleteMethod() {
        return new Object[][] { 
		{ "tags/", "1", "{ \"idTag\": \"1\", \"uuid\" : \""+UUID.randomUUID().toString()+"\", \"category\" : \"1\", \"active\" : true }" , HttpStatus.OK } //TODO expected '200' but was '404'
        };
    }

    public Object[][] negativeTestDataDeleteMethod() {
        return new Object[][] { 
		{ "tags/", "-1", "{ \"idTag\": \"0\", \"uuid\" : \""+UUID.randomUUID().toString()+"\", \"category\" : \"1\", \"active\" : true }" , HttpStatus.NOT_FOUND } 
        };
    }

    @DataProvider(name = "get-test-data")
    public Object[][] dataProviderGetMethod() {
	return super.mergeTestData(this.positiveTestDataGetMethod(), this.negativeTestDataGetMethod());	
    }

    @DataProvider(name = "post-test-data")
    public Object[][] dataProviderPostMethod() {
	return super.mergeTestData(this.positiveTestDataPostMethod(), this.negativeTestDataPostMethod());	
    }

    @DataProvider(name = "put-test-data")
    public Object[][] dataProviderPutMethod() {
	return super.mergeTestData(this.positiveTestDataPutMethod(), this.negativeTestDataPutMethod());	
    }

    @DataProvider(name = "delete-test-data")
    public Object[][] dataProviderDeleteMethod() {
	return super.mergeTestData(this.positiveTestDataDeleteMethod(), this.negativeTestDataDeleteMethod());	
    }

    @CitrusTest
    @Test(dataProvider = "post-test-data")
    public void post(final String endpoint, final String payload, final HttpStatus expectedResponse){
	super.post(endpoint, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "get-test-data")
    public void get(final String endpoint, final String endpointParameter, final HttpStatus expectedResponse, final Integer expectedElements){
	super.get(endpoint, endpointParameter, expectedResponse, expectedElements);
    }

    @CitrusTest
    @Test(dataProvider = "put-test-data")
    public void put(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.put(endpoint, endpointParameter, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "delete-test-data")
    public void delete(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.delete(endpoint, endpointParameter, payload, expectedResponse);
    }

}
