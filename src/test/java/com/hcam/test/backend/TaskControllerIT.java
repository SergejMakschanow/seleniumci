package com.hcam.test.backend;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import com.consol.citrus.annotations.CitrusTest;
import org.springframework.http.HttpStatus;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Integration tests for the CRUD properties of the TaskController of the hcam_backend
 *
 * @author Schiko
 * @since 2019-05-07
 */
@Test
public class TaskControllerIT extends ControllerIT {

    public Object[][] positiveTestDataGetMethod() {
        return new Object[][] { 
		{ "tasks/current/" , "yvasylenko" , HttpStatus.OK , 1 }, //TODO expected '200' but was '404'
		{ "tasks" , "" , HttpStatus.OK , 1 }
        };
    }

    public Object[][] negativeTestDataGetMethod() {
        return new Object[][] { 
		{ "tasks/current/" , "test-get-user" , HttpStatus.NOT_FOUND , 0 }
	};
    }


    public Object[][] positiveTestDataPostMethod() {
        return new Object[][] { 
		{ "tasks" , "{ \"title\": \"test-post-Betten-neu-beziehen\", \"description\" : \"todo!\", \"idTaskTemplate\" : \"1\", \"startDate\" : \"2016-11-23 15:55:22.000\", \"dueDate\": \"2016-11-23 15:55:23.000\", \"idAsset\": 1, \"username\": \"test-post-user\" }" , HttpStatus.CREATED } // TODO expected '201' but was '400' (Bad-request)
        };
    }

    public Object[][] negativeTestDataPostMethod() {
        return new Object[][] { 
		{ "tasks" , "{}" , HttpStatus.BAD_REQUEST } //TODO expected '400' but was '201'
        };
    }


    public Object[][] positiveTestDataPutMethod() {
        return new Object[][] { 
		{ "tasks/" , "1" ,"{ \"title\": \"test-post-Betten-neu-beziehen\", \"description\" : \"todo!\", \"startDate\" : \"2016-11-23 15:55:22.000\", \"dueDate\": \"2016-11-23 15:55:23.000\", \"idAsset\": 1, \"username\": \"test-post-user\" }" , HttpStatus.OK },
		{ "tasks/checkpoints/" , "1" ,"{ \"idTask\": \"0\", \"checkpoints\" : [ \"idCheckpoint\" : \"0\", \"type\": \"Beacon\", \"name\": \"test-Beacon\", \"defaultValue\": \"test-put-ckeckpoint\", \"value\" : \"test-put-value\", \"description\" : \"test-put-description\"] }" , HttpStatus.NO_CONTENT }
        };
    }

    public Object[][] negativeTestDataPutMethod() {
        return new Object[][] { 
		{ "tasks/checkpoints/" , "-1" ,"{ \"idTask\": \"0\", \"checkpoints\" : [ \"idCheckpoint\" : \"0\", \"type\": \"Beacon\", \"name\": \"test-Beacon\", \"defaultValue\": \"test-put-ckeckpoint\", \"value\" : \"test-put-value\", \"description\" : \"test-put-description\"] }" , HttpStatus.NO_CONTENT }
        };
    }


    public Object[][] positiveTestDataDeleteMethod() {
        return new Object[][] { 
		{ "tasks/" , "1" ,"{ \"title\": \"test-post-Betten-neu-beziehen\", \"description\" : \"todo!\", \"idTaskTemplate\" : \"1\", \"startDate\" : \"2016-11-23 15:55:22.000\", \"dueDate\": \"2016-11-23 15:55:23.000\", \"idAsset\": 1, \"username\": \"test-post-user\" }" , HttpStatus.NO_CONTENT } 
        };
    }

    public Object[][] negativeTestDataDeleteMethod() {
        return new Object[][] { 

        };
    }

    @DataProvider(name = "get-test-data")
    public Object[][] dataProviderGetMethod() {
	return super.mergeTestData(this.positiveTestDataGetMethod(), this.negativeTestDataGetMethod());	
    }

    @DataProvider(name = "post-test-data")
    public Object[][] dataProviderPostMethod() {
	return super.mergeTestData(this.positiveTestDataPostMethod(), this.negativeTestDataPostMethod());	
    }

    @DataProvider(name = "put-test-data")
    public Object[][] dataProviderPutMethod() {
	return super.mergeTestData(this.positiveTestDataPutMethod(), this.negativeTestDataPutMethod());	
    }

    @DataProvider(name = "delete-test-data")
    public Object[][] dataProviderDeleteMethod() {
	return super.mergeTestData(this.positiveTestDataDeleteMethod(), this.negativeTestDataDeleteMethod());	
    }

    @CitrusTest
    @Test(dataProvider = "post-test-data")
    public void post(final String endpoint, final String payload, final HttpStatus expectedResponse){
	super.post(endpoint, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "get-test-data")
    public void get(final String endpoint, final String endpointParameter, final HttpStatus expectedResponse, final Integer expectedElements){
	super.get(endpoint, endpointParameter, expectedResponse, expectedElements);
    }

    @CitrusTest
    @Test(dataProvider = "put-test-data")
    public void put(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.put(endpoint, endpointParameter, payload, expectedResponse);
    }

    @CitrusTest
    @Test(dataProvider = "delete-test-data")
    public void delete(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	super.delete(endpoint, endpointParameter, payload, expectedResponse);
    }

}
